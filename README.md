# docker-pushrm

Custom image for [christian-korneck/docker-pushrm](https://github.com/christian-korneck/docker-pushrm)
with an additional `entrypoint.sh`.

## About

> `docker-pushrm` is a Docker CLI plugin that adds a new `docker pushrm`
> (speak: "push readme") command to Docker.
>
> It pushes the README file from the current working directory to a
> container registry server where it appears as repo description in
> the webinterface.

## Why this image

This custom image adds an entrypoint script for the sole benefit of
saving a single line in my job definition
([read more](https://github.com/christian-korneck/docker-pushrm/issues/15)).

See the job definition in the [`.gitlab-ci.yml` file](.gitlab-ci.yml).

## Where

* [GitLab repository](https://gitlab.com/l3tompouce/docker/docker-pushrm)
* [Docker Hub repository](https://hub.docker.com/r/letompouce/docker-pushrm)
